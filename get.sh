


DIR=$1

for i in $(ls -tr $DIR); do
    if [ -d "$i" ]; then
        continue
    fi
    echo $i
    cat $DIR/$i/original_files/experiments_list.csv
    cat $DIR/$i/original_files/ew_param.csv
    cat $DIR/$i/original_files/wl_churn.csv
    cat $DIR/$i/original_files/wl_app_char.csv
    cat $DIR/$i/stdout.txt | grep fulf | wc -l
    mkdir $i
    cd $i
    ~/phd/reconfig_tests/all.sh -d $DIR/$i
    cd ..
    echo ""
done
